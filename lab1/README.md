#### Адресация, порты и фаерволл
8 апреля <br>
- разобрать структуру,нарисовать схему сети, расписать табличку подсетей;
- Найти свободные подсети;
  Подсчитать, сколько узлов в каждой подсети, включая свободные;
  Указать broadcast-адрес для каждой подсети;
  Проверить, нет ли ошибок при разбиении.
- Все серверы и роутеры должны ходить в Интернет через inetRouter;
  Все серверы должны видеть друг друга;
  У всех новых серверов отключить дефолт на NAT (eth0), который Vagrant поднимает для связи;
- поднять nginx на centralServer1;
  запустить на централСервер нжинкс - и настроить проброс - чтобы через localhost:8080 с хостовой машины можно было на него попасть
 
 #### Схема сети 
![Image](https://gitlab.com/ArkovKonstantin/mai-network/raw/master/images/schema.png)
#### Проверка доступности внешней сети с сервера office2Server
![Image](https://gitlab.com/ArkovKonstantin/mai-network/raw/master/images/office2Server.png)<br>
Как видно пинг во внешнюю сеть проходит. И утилита `tracepath` отображает прохождение пакетов по цепочке серверов, указанных на схеме.
#### Текущие правила маршрутизации для цепочки серверов указанной выше
- office2Server <br>
![Image](https://gitlab.com/ArkovKonstantin/mai-network/raw/master/images/1.png)
- office2Router<br>
![Image](https://gitlab.com/ArkovKonstantin/mai-network/raw/master/images/2.png)
- centralRouter<br>
![Image](https://gitlab.com/ArkovKonstantin/mai-network/raw/master/images/3.png)
- innetRouter<br>
![Image](https://gitlab.com/ArkovKonstantin/mai-network/raw/master/images/4.png)
#### Настройка правил для доступа к officeServer1 по 80 порту через хостовую машину по порту 8081
- даем доступ к inetRouter через localhost (Vagrantfile)<br>
![Image](https://gitlab.com/ArkovKonstantin/mai-network/raw/master/images/vm.png)
- добавляем в таблицу nat правила в цепочки PREROUTING и POSTROUTING<br>
![Image](https://gitlab.com/ArkovKonstantin/mai-network/raw/master/images/iptables.png)
- делаем запрос курлом на officeServer1. Как видим запрос проходит и nginx отдает страницу <br>
![Image](https://gitlab.com/ArkovKonstantin/mai-network/raw/master/images/check-nginx.png)
